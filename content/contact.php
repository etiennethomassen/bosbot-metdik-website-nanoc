
<div class="container">

<div class="row">
	<div class="span12">
		<h3 id="contact">Contact</h3>
		<p><strong>Bos</strong>bot <em>metdik</em> is een persoonlijk project van <a href="http://www.etiennethomassen.nl" title="Website van Etiënne Thomassen">Etiënne Thomassen</a>.
		</p>
	</div>
</div>
<div class="row">
	<div class="span12">
	<h4>Contactformulier</h4>
	<?php  
	        // check for a successful form post  
	        if (isset($_GET['s'])) echo "<div class=\"alert alert-success\">".$_GET['s']."</div>";  
	  
	        // check for a form error  
	        elseif (isset($_GET['e'])) echo "<div class=\"alert alert-error\">".$_GET['e']."</div>";  
	?>
</div>
	
	<form method="POST" action="contact-form-submission.php" class="well span10">
		<div class="row">
		<div class="span4">
		<label>Voornaam</label>
		<input type="text" name="voornaam" class="span4" placeholder="Voornaam">
		<label>Achternaam</label>
		<input type="text" name="achternaam" class="span4" placeholder="Achternaam">
		<label>Emailadres</label>
		<div class="input-prepend">
			<span class="add-on"><i class="icon-envelope"></i></span>
			<input type="text" name="email" id="inputIcon" class="span2" style="width:233px" placeholder="Emailadres">
		</div>
		<label>Onderwerp
		<select id="subject" name="onderwerp" class="span4">
		<option value="na" selected="">Kies:</option>
		<option value="service">Ondersteuning</option>
		<option value="suggestie">Suggestie</option>
		<option value="bug">Bug/fout in programma</option>
		<option value="overig">Anders</option>
		</select>
		</label>
		</div>
		<div class="span6">
		<label>Bericht</label>
		<textarea name="bericht" id="message" class="input-xlarge span6" rows="10"></textarea>
		</div>
		</div>
		<input type="hidden" name="save" value="contact"> 
		<button name="submit" type="submit" class="btn btn-primary pull-right">Versturen</button>
	</form>
</div>

